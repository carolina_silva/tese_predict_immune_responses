This is the work done during my master's dissertation - "Predict immune responses to neoantigens using machine learning".
The main goal of this work is to create machine learning models able to predict if neoantigens can trigger an immunologic response and, therefore, can be used for immunotherapy to treat cancer. 
This dissertation presents a machine learning pipeline that is able to identify neoantigens that can generate an immune response. 
Several machine learning models, built over different datasets were used to achieve this. 
The machine learning algorithms used were Support Vector Machines, K-nearest neighbors, Logistic Regression and Random Forests. 

To create the positive dataset, an extensive literature review on immunogenicity of neoantigens was performed. 
Then an SQLite database, called neoDB with annotated immunogenic neoantigens was created. To view the neoDB.db please use the website https://inloop.github.io/sqlite-viewer/.

The effect of hyperparameter tuning on model performance was assessed (using GridSearchCV and RandomizedSearchCV).
With three different sized datasets it was assessed whether there is an effect of the number of negative cases on model performance (Dataset1p1, Dataset1p2 and Dataset1p3).
The synthetic minority oversampling technique (SMOTE) was also used to create new cases for the minority class. 
