library(magrittr)
library(data.table)
library(antigen.garnish)

Sys.setenv(AG_DATA_DIR='/antigen.garnish')
print( Sys.getenv("AG_DATA_DIR"))

check_pred_tools()

indata<- read.table("e7.txt",sep = "\t",as.is = T, header = T)

dt <- garnish_affinity(dt = as.data.table(indata), binding_cutoff = 500000, remove_wt = FALSE) %T>%
str
