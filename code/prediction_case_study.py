#Predicting on case study data
#preprocessing
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile

df_lumc = pd.read_excel("case_study.xlsx")
df_lumc = df_lumc.drop(columns = "t_cell_type")
df_lumc = df_lumc.drop(columns = "hla")
df_lumc = df_lumc.drop(columns = 'mut_peptide')

df_lumc = df_lumc.drop_duplicates()

#Assign outcome as 0 if immunogenicity in "no" and 1 if "yes"
df_lumc['immunogenicity'] = [0 if x =='no' else 1 for x in df_lumc['immunogenicity']]

#Assing mut_in_anchor 0 if its "no" and 1 if yes 
df_lumc['mut_in_anchor'] = [0 if x == "no" else 1 for x in df_lumc['mut_in_anchor']]

#Assign X as a dataframe of features and y as a series of outcome variables
w = df_lumc.drop('immunogenicity',1)
z = df_lumc.immunogenicity

#handling missing data
print(w.isna().sum().sort_values(ascending = False).head())

from missingpy import KNNImputer
import numpy as np
import warnings
warnings.filterwarnings('ignore')
nan = np.nan
imputer = KNNImputer(n_neighbors=5)
w = pd.DataFrame(imputer.transform(w),columns = w.columns)

#scaling
from sklearn.preprocessing import StandardScaler 
df_col = X_train.drop(columns = "mut_in_anchor")
w[['grantham_score','hydrophobicity','polarity','score_netmhc','%rank_netmhc','score_netmhcpan',
                         '%rank_netmhcpan','ensemble_score','dissimilarity','ctl_tap','ctl_cleavage','ctl_combined','ctl_rank','dai',
                         'similarity']] = pd.DataFrame(scaler.transform(w[['grantham_score','hydrophobicity','polarity','score_netmhc','%rank_netmhc','score_netmhcpan',
                         '%rank_netmhcpan','ensemble_score','dissimilarity','ctl_tap','ctl_cleavage','ctl_combined','ctl_rank','dai',
                         'similarity']]), columns = df_col.columns)


#prediction - this was done twice. One time using Dataset1p1 as training data and other time using DatasetSMOTE as training data
z_true, z_pred = z, model_rf.predict(w)

print(z_pred)

from sklearn.metrics import confusion_matrix
print("CONFUSION MATRIX")
print(confusion_matrix(z_true,z_pred))

